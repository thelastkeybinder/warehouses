<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 19:33
 */

namespace Warehouses\src;

use Warehouses\src\Exceptions\ItemCreationException;

include_once "CustomItem1.php";
include_once "CustomItem2.php";
include_once "Exceptions/ItemCreationException.php";

class ItemFactory
{
    /**
     * @param string $itemType
     * @param int $id
     * @param string $name
     * @param Brand $brand
     * @param array $other
     * @return Item
     * @throws ItemCreationException
     */
    public static function createItem(string $itemType, int $id, string $name, Brand $brand, array $other): Item
    {
        switch ($itemType){
            case "CustomItem1":
                return new CustomItem1($id, $name, $brand, $other["color"], $other["weight"]);
                break;
            case "CustomItem2":
                return new CustomItem2($id, $name, $brand, $other["width"], $other["height"]);
                break;
            default:
                throw new ItemCreationException("Unknown item type $itemType");
        }
    }

    /**
     * Creates an array of the given item
     *
     * @param Item $stockItem
     * @param $quantity
     * @return array
     */
    public static function copyItem(Item $stockItem, $quantity): array
    {
        $ret = [$stockItem];
        for($i=0; $i < $quantity - 1; $i++){
            array_push($ret, clone($stockItem));
        }
        return $ret;
    }
}