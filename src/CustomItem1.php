<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 19:24
 */

namespace Warehouses\src;

include_once "Item.php";

class CustomItem1 extends Item
{
    /**
     * @var string
     */
    private $color;

    /**
     * @var int
     */
    private $weight;

    /**
     * CustomItem1 constructor.
     * @param int $id
     * @param string $name
     * @param Brand $brand
     * @param string $color
     * @param int $height
     */
    public function __construct($id, $name, Brand $brand, string $color, int $height)
    {
        parent::__construct($id, $name, $brand);

        $this->color = $color;
        $this->weight = $height;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    public function __toString(): string
    {
        return sprintf("%s Color: %s, weight: %s",parent::__toString(), $this->color, $this->weight);
    }
}