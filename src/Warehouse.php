<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 18:43
 */

namespace Warehouses\src;

use Warehouses\src\Exceptions\WarehouseException;

include_once "Exceptions/WarehouseException.php";

class Warehouse
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $address;
    /**
     * @var int
     */
    private $capacity;

    /**
     * @var array of associative arrays. Each array looks like ["quantity"=>(int)q, "item"=>(Item) i]
     */
    private $stock = [];

    /**
     * Warehouse constructor.
     * @param string $name
     * @param string $address
     * @param int $capacity
     */
    public function __construct(string $name, string $address, int $capacity = 0){
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity(int $capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getOccupiedSpace(): int
    {
        return array_sum(array_map(function($item) {
            return $item['quantity'];
        }, $this->stock));
    }

    public function getFreeSpace(): int
    {
        return $this->capacity - $this->getOccupiedSpace();
    }

    /**
     * @param Item $item
     * @param int $quantity
     * @throws WarehouseException
     */
    public function addItem(Item $item, int $quantity = 1)
    {
        if ($quantity > $this->getFreeSpace()){
            throw new WarehouseException(
                "Trying to add $quantity item but free capacity is only {$this->getFreeSpace()}");
        }

        $found = false;
        foreach ($this->stock as $stockItem){
            if ($stockItem["item"]->equals($item)){
                $stockItem["quantity"]+=$quantity;
                break;
            }
        }
        if (!$found){
            array_push($this->stock, ["quantity" => $quantity, "item" => $item]);
        }

    }

    /**
     * @param int $id
     * @param int $quantity
     * @return array
     * @throws WarehouseException
     */
    public function getItemById(int $id, int $quantity = 1): array
    {
        foreach ($this->stock as $index => $stockItem){
            if ($stockItem["item"]->getId() == $id){
                if($stockItem["quantity"] < $quantity){
                    throw new WarehouseException(
                        "There is not enough pieces of the requested item. Requested $quantity, got 
                        {$stockItem["quantity"]}");
                }
                else{
                    $this->stock[$index]["quantity"] -= $quantity;
                    return ItemFactory::copyItem($stockItem["item"], $quantity);
                }
            }
        }
        throw new WarehouseException("Item with id $id not found in the warehouse.");
    }

    /**
     * @param int $id
     * @return int
     */
    public function getItemCountById(int $id): int
    {
        foreach ($this->stock as $stockItem){
            if ($stockItem["item"]->getId() == $id){
                return $stockItem["quantity"];
            }
        }
        return 0;
    }

    public function printAll()
    {
        print_r((string)$this . PHP_EOL);
        foreach($this->stock as $stockItem){
            print_r((string) $stockItem["item"] . " ({$stockItem["quantity"]} pcs)" . PHP_EOL);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("Warehouse::%s, at %s. Capacity: %d/%d", $this->name, $this->address, $this->getFreeSpace(), $this->capacity);
    }
}