<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 19:02
 */

namespace Warehouses\src;

class Item
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * @var Brand
     */
    protected $brand;

    /**
     * Item constructor.
     * @param int $id
     * @param string $name
     * @param Brand $brand
     */
    public function __construct(int $id, string $name, Brand $brand)
    {
        $this->id = $id;
        $this->name = $name;
        $this->brand = $brand;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf("Item::Id:%d name:%s brand:%s", $this->id, $this->name, $this->brand->getName());
    }

    public function equals(Item $other): bool
    {
        return $this->id == $other->getId();
    }
}