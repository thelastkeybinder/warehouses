<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 19:24
 */

namespace Warehouses\src;

include_once "Item.php";

class CustomItem2 extends Item
{
    /**
     * @var string
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * CustomItem2 constructor.
     * @param int $id
     * @param string $name
     * @param Brand $brand
     * @param string $width
     * @param int $height
     * @internal param string $color
     */
    public function __construct($id, $name, Brand $brand, string $width, int $height)
    {
        parent::__construct($id, $name, $brand);

        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getWidth(): string
    {
        return $this->width;
    }

    /**
     * @param string $width
     */
    public function setWidth(string $width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    public function __toString(): string
    {
        return sprintf("%s Width: %s, height: %s",parent::__toString(), $this->width, $this->height);
    }
}