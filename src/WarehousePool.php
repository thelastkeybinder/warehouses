<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 22:00
 */

namespace Warehouses\src;

use Warehouses\src\Exceptions\WarehousePoolException;

include_once "Exceptions/WarehousePoolException.php";

class WarehousePool
{
    /**
     * @var array
     */
    private $pool = [];

    /**
     * @param Warehouse $w
     */
    public function addWarehouse(Warehouse $w)
    {
        array_push($this->pool, $w);
        print_r("Warehouse added to pool: " . $w->getName() . PHP_EOL);
    }

    /**
     * @param Item $item
     * @param int $quantity
     * @throws WarehousePoolException
     */
    public function addItemToWarehouse(Item $item, int $quantity = 1)
    {
        $free_space_sum = 0;
        foreach($this->pool as $wh){
            $free_space_sum += $wh->getFreeSpace();
            if($free_space_sum > $quantity){
                break;
            }
        }
        if($free_space_sum < $quantity){
            throw new WarehousePoolException("There are not enough space in the warehouses.");
        }

        foreach($this->pool as $wh){
            $free_space = $wh->getFreeSpace();
            if($free_space >= $quantity){
                $wh->addItem($item,$quantity);
                break;
            }
            else{
                $wh->addItem($item,$free_space);
                $quantity -= $free_space;
            }
        }
    }

    /**
     * @param int $id
     * @param int $quantity
     * @return array
     * @throws WarehousePoolException
     * @internal param Item $item
     */
    public function getItemFromWarehouse(int $id, int $quantity = 1): array
    {
        //First check if there is enough items
        $item_sum = 0;
        foreach($this->pool as $wh){
            $item_sum += $wh->getItemCountById($id);
            if($item_sum >= $quantity){
                break;
            }
        }
        if($item_sum < $quantity){
            throw new WarehousePoolException("There are not enough items with id $id in the warehouses.");
        }
        $ret = [];
        foreach($this->pool as $wh){
            $item_count = $wh->getItemCountById($id);
            if($item_count >= $quantity){
                array_merge($ret, $wh->getItemById($id,$quantity));
                break;
            }
            else{
                array_merge($ret, $wh->getItemById($id,$item_count));
                $quantity -= $item_count;
            }
        }
        return $ret;
    }

    public function printAll(){
        print_r(PHP_EOL . "===== Warehouse database start======" . PHP_EOL);
        foreach($this->pool as $wh){
            $wh->printAll();
        }
        print_r("===== Warehouse database end======" . PHP_EOL . PHP_EOL);
    }
}