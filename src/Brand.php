<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 18:58
 */

namespace Warehouses\src;

use Warehouses\src\Exceptions\BrandException;

include_once "Exceptions/BrandException.php";

class Brand
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $quality;

    /**
     * Brand constructor.
     * @param string $name
     * @param int $quality
     * @throws BrandException
     */
    public function __construct(string $name, int $quality)
    {
        $this->name = $name;
        if ($quality > 0 && $quality < 6){
            $this->quality = $quality;
        }
        else{
            throw new BrandException("Quality should be between 1 and 5. Got $quality");
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality(int $quality)
    {
        $this->quality = $quality;
    }

    public function __toString(): string
    {
        return sprintf("Brand::%s(%d)",$this->name, $this->quality);
    }
}