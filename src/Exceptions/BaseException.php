<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 19:44
 */

namespace Warehouses\src\Exceptions;

class BaseException extends \Exception
{
    public function __toString(): string
    {
        return "[{$this->code}]: {$this->message}\n";
    }
}