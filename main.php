<?php
/**
 * User: Nagy-Mengyi Zsolt
 * Date: 2017.11.06.
 * Time: 18:50
 */
namespace Warehouses;

use Warehouses\src\{
    Exceptions\BrandException, Exceptions\WarehousePoolException, WarehousePool, Warehouse, ItemFactory, Brand
};

include_once "src/WarehousePool.php";
include_once "src/Warehouse.php";
include_once "src/ItemFactory.php";
include_once "src/Brand.php";


if ($argc < 3){
    printHelp();
    exit;
}

switch ($argv[2]){
    case 1:
        test1($argv[1]);
        break;
    case 2:
        test2($argv[1]);
        break;
    case 3:
        test3($argv[1]);
        break;
    default:
        printHelp();
        exit;
}

/**
 * Add x items, get y items (x > y)
 * @param string $configFile
 */
function test1(string $configFile){
    $warehouses =[];
    $brands =[];
    $items =[];
    readConfig($warehouses, $brands, $items, $configFile);

    $pool = new WarehousePool();
    $pool->addWarehouse($warehouses[0]);
    $pool->addWarehouse($warehouses[1]);

    try{
        print_r("Adding items..." . PHP_EOL);
        $pool->addItemToWarehouse($items[0],8);
        $pool->addItemToWarehouse($items[1],10);
        $pool->printAll();
    }
    catch (WarehousePoolException $e){
        print_r("Could not add items: " . $e->getMessage() . PHP_EOL);
    }

    try{
        print_r("Retrieving items..." . PHP_EOL);
        $items = $pool->getItemFromWarehouse($items[1]->getId(), 4);
    }
    catch (WarehousePoolException $e){
        print_r("Could not get items: " . $e->getMessage() . PHP_EOL);
    }

    foreach($items as $item){
        print_r((string)$item . PHP_EOL);
    }

    $pool->printAll();
}

/**
 * Add x items but not enough space
 * @param string $configFile
 */
function test2(string $configFile){
    $warehouses =[];
    $brands =[];
    $items =[];
    readConfig($warehouses, $brands, $items, $configFile);

    $pool = new WarehousePool();
    $pool->addWarehouse($warehouses[0]);
    $pool->addWarehouse($warehouses[1]);

    try{
        print_r("Adding items..." . PHP_EOL);
        $pool->addItemToWarehouse($items[0],8);
        $pool->addItemToWarehouse($items[1],40);
        $pool->printAll();

        print_r("Retrieving items..." . PHP_EOL);
    }
    catch (WarehousePoolException $e){
        print_r("Could not add items: " . $e->getMessage() . PHP_EOL);
    }

    $pool->printAll();
}

/**
 * Add x items, get y items (x < y)
 * @param string $configFile
 */
function test3(string $configFile){
    $warehouses =[];
    $brands =[];
    $items =[];
    readConfig($warehouses, $brands, $items, $configFile);

    $pool = new WarehousePool();
    $pool->addWarehouse($warehouses[0]);
    $pool->addWarehouse($warehouses[1]);

    try{
        print_r("Adding items..." . PHP_EOL);
        $pool->addItemToWarehouse($items[0],8);
        $pool->addItemToWarehouse($items[1],10);
        $pool->printAll();
    }
    catch (WarehousePoolException $e){
        print_r("Could not add items: " . $e->getMessage() . PHP_EOL);
    }

    try{
        print_r("Retrieving items..." . PHP_EOL);
        $items = $pool->getItemFromWarehouse($items[1]->getId(), 14);
    }
    catch (WarehousePoolException $e){
        print_r("Could not get items: " . $e->getMessage() . PHP_EOL);
    }

    foreach($items as $item){
        print_r((string)$item . PHP_EOL);
    }

    $pool->printAll();
}

/**
 * Initializes the main types from the configuration file
 * @param array $warehouses
 * @param array $brands
 * @param array $items
 * @param $configFile
 */
function readConfig(array &$warehouses, array &$brands, array &$items, $configFile)
{
    print_r("===== Config parse START ======" . PHP_EOL);
    $config = json_decode(file_get_contents($configFile), true);
    foreach($config["warehouses"] as $wh){
        $whToPush = new Warehouse($wh["name"],$wh["address"],$wh["capacity"]);
        array_push($warehouses, $whToPush);
        print_r("Added " . (string) $whToPush . PHP_EOL);
    }

    foreach($config["brands"] as $brand){
        try{
            $brandToPush = new Brand($brand["name"], $brand["quality"]);
            array_push($brands, $brandToPush);
            print_r("Added " . (string) $brandToPush . PHP_EOL);
        }
        catch(BrandException $e){
            print_r("Could not add brand: " . $e->getMessage() . PHP_EOL);
        }
    }

    foreach($config["items"] as $item){
        $baseProperties = [
            "type" => $item["type"],
            "name" => $item["name"],
            "id" => $item["id"],
            "brand" => $brands[$item["brand"]]
        ];
        unset($item["type"]);
        unset($item["name"]);
        unset($item["id"]);
        unset($item["brand"]);

        $itemToPush = ItemFactory::createItem($baseProperties["type"], $baseProperties["id"],
            $baseProperties["name"], $baseProperties["brand"], $item);
        array_push($items, $itemToPush);
        print_r("Added " . (string)$itemToPush . PHP_EOL);
    }
    print_r("===== Config parse END ======" . PHP_EOL);
}

function printHelp()
{
    print_r("Usage: php main.php [config file path] [test number 1-3]");
}

