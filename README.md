**Otthoni teszt**<br/>
Adott eset lejátszásához a main.php fájlt kell felhívni a következő módon:<br/>
`php main.php config/test1.json [teszt száma]`<br/>
Ahol a teszt száma 1, 2 vagy 3 lehet.

Megjegyzések:
- A feladat elkészítéséhez PHP 7.0-t használtam.
- Bár nem volt megkötve, de feltételeztem, hogy egy terméket a cikkszám (id) egyértelműen azonosít. 
- Elemek elhelyezésekor és kivételekor a program először ellenőrzi a raktárakat. Ha nincs elég hely elhelyezéshez, vagy nincs annyi elem amennyit ki szeretnénk venni, akkor nem hajtódik végre a művelet.